using System.Data.SQLite;
using System.IO;
using System;




namespace Contexter
{
	internal partial class Database
	{
		private static readonly string[] FILE_NAMES = {
			".contexter",
			".contexter.db",
			".contexter.db3",
			".contexter.sqlite",
			".contexter.sqlite3",
		};




		public static bool TryLoadContext(string startingDirectory)
		{
			if (!TryFindContextFile(startingDirectory, out string ffff)) return false;

			_connection = new SQLiteConnection($"Data Source={ffff};Version=3;");

			return true;
		}


		public static bool TryFindContextFile(string directory, out string path)
		{
			Console.Error.WriteLine($"Searching for database file at {directory}...");

			foreach (string fileName in FILE_NAMES) {
				string possiblePath = Path.Combine(directory, fileName);
				if (File.Exists(possiblePath)) {
					path = possiblePath;
					return true;
				}
			}

			path = "";
			return (directory != Directory.GetDirectoryRoot(directory)) &&
			       TryFindContextFile(Directory.GetParent(directory)!.ToString(), out path);
		}
	}
}




/*                                                                                            */
/*          ______                               _______                                      */
/*          \  __ \____  ____________  ______ ___\  ___/_____________  ____  ____ ___         */
/*          / /_/ / __ \/ ___/ ___/ / / / __ \__ \\__ \/ ___/ ___/ _ \/ __ \/ __ \__ \        */
/*         / ____/ /_/ /__  /__  / /_/ / / / / / /__/ / /__/ /  / ___/ /_/ / / / / / /        */
/*        /_/    \____/____/____/\____/_/ /_/ /_/____/\___/_/   \___/\__/_/_/ /_/ /__\        */
/*                                                                                            */
/*        Licensed under the Apache License, Version 2.0. See LICENSE.md for more info        */
/*        David Tabernero M. @ PossumScream                      Copyright © 2021-2023        */
/*        GitLab - GitHub: possumscream                            All rights reserved        */
/*        -------------------------                                  -----------------        */
/*                                                                                            */