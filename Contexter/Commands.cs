﻿using System;


namespace Contexter
{
	internal class Commands
	{
		private const string OK = "OK";
		private const string KO = "KO";


		public static void Run()
		{
			string key, value;
			while (Arguments.TryGetNext(out string command)) {
				switch (command.ToUpper()) {
					case "GET":
						if (!Arguments.TryGetNext(out key)) return;
						if (Database.TryGet(key, out value)) {
							Console.Out.WriteLine(value);
							Console.Error.WriteLine(OK);
						}
						else {
							Console.Error.WriteLine(KO);
						}
						break;

					case "SET":
						if (!Arguments.TryGetNext(out key)) return;
						if (!Arguments.TryGetNext(out value)) return;
						Console.Error.WriteLine(Database.TrySet(key, value) ? OK : KO);
						break;

					case "DEL":
						if (!Arguments.TryGetNext(out key)) return;
						Console.Error.WriteLine(Database.TryDelete(key) ? OK : KO);
						break;

					default:
						Console.Error.WriteLine($"Command {command.ToUpper()} not valid");
						break;
				}
			}
		}
	}
}


/*                                                                                            */
/*          ______                               _______                                      */
/*          \  __ \____  ____________  ______ ___\  ___/_____________  ____  ____ ___         */
/*          / /_/ / __ \/ ___/ ___/ / / / __ \__ \\__ \/ ___/ ___/ _ \/ __ \/ __ \__ \        */
/*         / ____/ /_/ /__  /__  / /_/ / / / / / /__/ / /__/ /  / ___/ /_/ / / / / / /        */
/*        /_/    \____/____/____/\____/_/ /_/ /_/____/\___/_/   \___/\__/_/_/ /_/ /__\        */
/*                                                                                            */
/*        Licensed under the Apache License, Version 2.0. See LICENSE.md for more info        */
/*        David Tabernero M. @ PossumScream                      Copyright © 2021-2023        */
/*        GitLab - GitHub: possumscream                            All rights reserved        */
/*        -------------------------                                  -----------------        */
/*                                                                                            */