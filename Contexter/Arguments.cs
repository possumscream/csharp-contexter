namespace Contexter
{
	internal class Arguments
	{
		private static string[] _values = {};
		private static int _currentIndex = -1;


		public static void Store(string[] args)
		{
			_values = args;
		}


		public static void Reset()
		{
			_currentIndex = -1;
		}


		public static bool TryGetNext(out string argument)
		{
			if (++_currentIndex < _values.Length) {
				argument = _values[_currentIndex];
				return true;
			}
			else {
				argument = "";
				return false;
			}
		}


		public static string[] values => _values;
	}
}


/*                                                                                            */
/*          ______                               _______                                      */
/*          \  __ \____  ____________  ______ ___\  ___/_____________  ____  ____ ___         */
/*          / /_/ / __ \/ ___/ ___/ / / / __ \__ \\__ \/ ___/ ___/ _ \/ __ \/ __ \__ \        */
/*         / ____/ /_/ /__  /__  / /_/ / / / / / /__/ / /__/ /  / ___/ /_/ / / / / / /        */
/*        /_/    \____/____/____/\____/_/ /_/ /_/____/\___/_/   \___/\__/_/_/ /_/ /__\        */
/*                                                                                            */
/*        Licensed under the Apache License, Version 2.0. See LICENSE.md for more info        */
/*        David Tabernero M. @ PossumScream                      Copyright © 2021-2023        */
/*        GitLab - GitHub: possumscream                            All rights reserved        */
/*        -------------------------                                  -----------------        */
/*                                                                                            */