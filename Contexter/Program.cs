﻿using System.IO;
using System;


namespace Contexter
{
	internal class Program
	{
		public static void Main(string[] args)
		{
			// Load defaults
			string currentDirectory = Directory.GetCurrentDirectory();
			Console.Error.WriteLine($"Working by default on {currentDirectory}...");

			if (Database.TryLoadContext(currentDirectory))
				Console.Error.WriteLine("Database loaded");
			else
				Console.Error.WriteLine("No database found");


			// Process argument commands
			Arguments.Store(args);

			Database.Open();
			Commands.Run();
			Database.Close();

			Console.Error.WriteLine("End");
		}
	}
}


/*                                                                                            */
/*          ______                               _______                                      */
/*          \  __ \____  ____________  ______ ___\  ___/_____________  ____  ____ ___         */
/*          / /_/ / __ \/ ___/ ___/ / / / __ \__ \\__ \/ ___/ ___/ _ \/ __ \/ __ \__ \        */
/*         / ____/ /_/ /__  /__  / /_/ / / / / / /__/ / /__/ /  / ___/ /_/ / / / / / /        */
/*        /_/    \____/____/____/\____/_/ /_/ /_/____/\___/_/   \___/\__/_/_/ /_/ /__\        */
/*                                                                                            */
/*        Licensed under the Apache License, Version 2.0. See LICENSE.md for more info        */
/*        David Tabernero M. @ PossumScream                      Copyright © 2021-2023        */
/*        GitLab - GitHub: possumscream                            All rights reserved        */
/*        -------------------------                                  -----------------        */
/*                                                                                            */