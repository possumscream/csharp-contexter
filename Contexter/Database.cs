using System.Data.SQLite;
using System.Data;




namespace Contexter
{
	internal partial class Database
	{
		private const string DATA_TABLE_NAME = "data";
		private const string KEY_COLUMN_NAME = "key";
		private const string VALUE_COLUMN_NAME = "value";

		private const string SELECTION_COMMAND = @"SELECT (`{2}`) FROM `{0}` WHERE `{1}`=@key;";
		private const string INSERTION_COMMAND = @"INSERT OR REPLACE INTO `{0}`(`{1}`,`{2}`) VALUES (@key,@val);";
		private const string DELETION_COMMAND = @"DELETE FROM `{0}` WHERE `{1}`=@key;";


		private static SQLiteConnection _connection = new();




		public static void Open()
		{
			_connection.Open();
		}


		public static void Close()
		{
			_connection.Close();
		}




		public static bool TryGet(string key, out string value)
		{
			SQLiteCommand command = new(
				string.Format(SELECTION_COMMAND, DATA_TABLE_NAME, KEY_COLUMN_NAME, VALUE_COLUMN_NAME),
				_connection);

			command.Parameters.Add("@key", DbType.String).Value = key;

			SQLiteDataReader reader = command.ExecuteReader();
			if (reader.Read()) {
				value = reader.GetString(0);
				return true;
			}
			else {
				value = "";
				return false;
			}
		}


		public static bool TrySet(string key, string value)
		{
			SQLiteCommand command = new(
				string.Format(INSERTION_COMMAND, DATA_TABLE_NAME, KEY_COLUMN_NAME, VALUE_COLUMN_NAME),
				_connection);

			command.Parameters.Add("@key", DbType.String).Value = key;
			command.Parameters.Add("@val", DbType.String).Value = value;

			return (command.ExecuteNonQuery() == 1);
		}


		public static bool TryDelete(string key)
		{
			SQLiteCommand command = new(
				string.Format(DELETION_COMMAND, DATA_TABLE_NAME, KEY_COLUMN_NAME),
				_connection);

			command.Parameters.Add("@key", DbType.String).Value = key;

			return (command.ExecuteNonQuery() == 1);
		}
	}
}




/*                                                                                            */
/*          ______                               _______                                      */
/*          \  __ \____  ____________  ______ ___\  ___/_____________  ____  ____ ___         */
/*          / /_/ / __ \/ ___/ ___/ / / / __ \__ \\__ \/ ___/ ___/ _ \/ __ \/ __ \__ \        */
/*         / ____/ /_/ /__  /__  / /_/ / / / / / /__/ / /__/ /  / ___/ /_/ / / / / / /        */
/*        /_/    \____/____/____/\____/_/ /_/ /_/____/\___/_/   \___/\__/_/_/ /_/ /__\        */
/*                                                                                            */
/*        Licensed under the Apache License, Version 2.0. See LICENSE.md for more info        */
/*        David Tabernero M. @ PossumScream                      Copyright © 2021-2023        */
/*        GitLab - GitHub: possumscream                            All rights reserved        */
/*        -------------------------                                  -----------------        */
/*                                                                                            */